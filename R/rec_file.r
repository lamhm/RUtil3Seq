

#' Load a 3seq.rec file
#' @description This function is to load a 3seq.rec file.
#' @param recFile    The path to the 3seq.rec file.
#' @param encoding   The character encoding of the file. Default: UTF-8.
#' @param columnSep  The special character that is used for column separators in
#'                   the 3seq.rec file.
#' @param decimalSep The character that is used for decimal separators.
#'                   Default: "."
#' @return A data frame containing the recombination table in the given .rec
#'         file.
#' @export
readRecFile <- function( recFile, encoding = "utf8",
                         columnSep = "\t", decimalSep = "." ) {

    ## Number of columns in the 3seq.rec file
    N_COLUMNS <- 13

    dataLines <- c()
    fileConnection = file(recFile, open = "rt", encoding = encoding)
    while ( TRUE ) {
        line = readLines(fileConnection, n = 1)
        if ( length(line) == 0 ) {
            ## End of file
            break
        }

        line <- trimws(line)
        fields <- unlist( strsplit(line, split = columnSep) )
        nFields <- length(fields)
        if (nFields > N_COLUMNS) {
            lastField <- paste(fields[N_COLUMNS : nFields], collapse = " | ")
            fields <- append( fields[1 : (N_COLUMNS - 1)], lastField )
        } else if (nFields < N_COLUMNS) {
            fields <- append(fields, rep("", N_COLUMNS - nFields))
        }
        line <- paste(fields, collapse = ",")

        dataLines <- append( dataLines, line )
    }
    close(fileConnection)

    recData <- read.csv( textConnection(dataLines),
                         header = T, stringsAsFactors = F,
                         sep = ",", dec = decimalSep )
    return(recData)
}
# data <- readRecFile("test-data/zika.3s.rec")



#' Plot break-point histogram
#' @description This function plots the number of time each nucleotide position
#'              appearing as a break-point.
#' @param recFile    The path to the 3seq.rec file.
#' @param encoding   The character encoding of the file. Default: UTF-8.
#' @param columnSep  The special character that is used for column separators in
#'                   the 3seq.rec file.
#' @param decimalSep The character that is used for decimal separators.
#'                   Default: "."
#' @param plotType   The type of break-points that need to be plotted.
#'                   Supported options: "any", "first", "second", "none".
#'                   Default: "any".
#' @return A data frame containing the recombination table in the given .rec
#'         file.
#' @export
breakpointHist <- function( recFile, encoding = "utf8",
                            columnSep = "\t", decimalSep = ".",
                            plotType = c("any", "first", "second", "none") ) {


    ## The column in the REC file that contains break-point positions
    BREAKPOINT_COLUMN <- 13

    recData <- readRecFile( recFile = recFile, encoding = encoding,
                            columnSep = columnSep, decimalSep = decimalSep )

    seqLen <- 0
    firstBreakpoint <- c(0)
    secondBreakpoint <- c(0)

    for (line in recData[, BREAKPOINT_COLUMN]) {
        bpPairs <- unlist( strsplit(line, split = " | ", fixed = T) )
        for (pair in bpPairs) {
            ranges <- unlist( strsplit(pair, split = " & ", fixed = T) )

            firstRange <- unlist( strsplit(ranges[1], split = "-", fixed = T) )
            firstRange <- trimws(firstRange)
            firstRange <- as.numeric(firstRange)

            secondRange <- unlist( strsplit(ranges[2], split = "-", fixed = T) )
            secondRange <- trimws(secondRange)
            secondRange <- as.numeric(secondRange)

            newLength <- max(firstRange, secondRange)
            if ( seqLen < newLength ) {
                lenDiff <- newLength - seqLen
                firstBreakpoint <- append( firstBreakpoint, rep(0, lenDiff) )
                secondBreakpoint <- append( secondBreakpoint, rep(0, lenDiff) )
                seqLen <- newLength
            }

            for ( position in (firstRange[1] : firstRange[2]) ) {
                firstBreakpoint[position + 1] <-
                    firstBreakpoint[position + 1] + 1
            }
            for ( position in (secondRange[1] : secondRange[2]) ) {
                secondBreakpoint[position + 1] <-
                    secondBreakpoint[position + 1] + 1
            }
        }
    }

    anyBreakpoint <- firstBreakpoint + secondBreakpoint
    position <- 0 : seqLen

    plotType <- plotType[1]
    if (plotType == "any") {
        plot(position, anyBreakpoint, type = "l", las = 2, lwd = 2,
             main = "Detected as Any Break-point Types",
             xlab = "Position", ylab = "Count")
    } else if (plotType == "first") {
        plot(position, firstBreakpoint, type = "l", las = 2, lwd = 2,
             main = "Detected as the First Break-point",
             xlab = "Position", ylab = "Count")
    } else if (plotType == "second") {
        plot(position, secondBreakpoint, type = "l", las = 2, lwd = 2,
             main = "Detected as the Second Break-point",
             xlab = "Position", ylab = "Count")
    }

    return(
        list(position = position, firstBreakpoint = firstBreakpoint,
             secondBreakpoint = secondBreakpoint, anyBreakpoint = anyBreakpoint)
    )
}

# breakpointHist("test-data/zika.3s.rec", plotType = "second")
# breakpointHist("test-data/zika.3s.rec", plotType = "first")





